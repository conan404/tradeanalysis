﻿using CurrentPositionAnalyser.Enums;
using CurrentPositionAnalyser.Models;

namespace CurrentPositionAnalyser.Interfaces.Services
{
    internal interface IIntersectionLineAnalyser
    {
        EBuyOrSell? AnalyseLines(IntersectionLine[] intersectionLines);
    }
}
