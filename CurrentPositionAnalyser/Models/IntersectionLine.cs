﻿using CurrentPositionAnalyser.Enums;

namespace CurrentPositionAnalyser.Models
{
    public class IntersectionLine
    {
        public decimal CurrentValue { get; set; }
        public decimal CurrentGradient { get; set; }
        public ELineType LineType { get; set; }
    }
}
