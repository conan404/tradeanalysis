﻿using CurrentPositionAnalyser.Enums;
using CurrentPositionAnalyser.Interfaces.Services;
using CurrentPositionAnalyser.Models;

namespace CurrentPositionAnalyser
{
    public interface ITradeAnalyser
    {
        EBuyOrSell? HowToTrade(IntersectionLine [] intersectionLines);
    }

    public class TradeAnalyser : ITradeAnalyser
    {
        private IIntersectionLineAnalyser _intersetionLineAnalyser;

        #region Constrcutors

        TradeAnalyser(IIntersectionLineAnalyser intersectionLineAnalyser)
        {
            _intersetionLineAnalyser = intersectionLineAnalyser;
        }

        #endregion

        public EBuyOrSell? HowToTrade(IntersectionLine [] intersectionLines)
        {
            //Update Last Line Interection time
            EBuyOrSell? buyOrSell = CheckLineIntersection(intersectionLines);

            if (!buyOrSell.HasValue)
                return buyOrSell;

            //Analyse general trend of stock for the day and if anaylysis action (buy or sell) matches day's trend i.e. up or down, continue, otherwise, do not action 
            EDayTrend? dayTrend = GetStockMarketTrendForToday();

            //Analyse average distance between previous intersections for day. If peridod is long enough for current needs, continue, otherwise do not action 

            //if here return EBuyrSell
            return buyOrSell;
        }

        #region private helpers

        private EBuyOrSell? CheckLineIntersection(IntersectionLine[] intersectionLines)
        {
            //Buy Or Sell
            return _intersetionLineAnalyser.AnalyseLines(intersectionLines);
        }

        private EDayTrend? GetStockMarketTrendForToday()
        {
            //TODO: Continue from here

            
            return null;
        }
            
        #endregion
    }
}
