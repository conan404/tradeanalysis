﻿using CurrentPositionAnalyser.Enums;
using CurrentPositionAnalyser.Models;
using System;
using System.Linq;

namespace CurrentPositionAnalyser.Services
{
    internal interface IIntersectionLineAnalyser
    {
        EBuyOrSell? AnalyseLines(IntersectionLine[] intersectionLines);
    }

    internal class SmaEmaLineIntersectionAnalyser : IIntersectionLineAnalyser
    {
        private const decimal MinGradientDiff = 1;

        public EBuyOrSell? AnalyseLines(IntersectionLine[] intersectionLines)
        {
            //If lines don't intersect, don't boy or sell
            if (!DoLinesIntersect(intersectionLines))
                return null;
           
            return ShouldBuyOrSell(intersectionLines);
        }

        private EBuyOrSell? ShouldBuyOrSell(IntersectionLine[] intersectionLines)
        {
            //Check Gradient Difference
            if (GetGradientDiff(intersectionLines) < MinGradientDiff)
                return null;

            IntersectionLine smaIntersectionLine = intersectionLines.Single(x => x.LineType == ELineType.SMA);
            IntersectionLine emaIntersectionLine = intersectionLines.Single(x => x.LineType == ELineType.EMA);

            //TODO: Look at this - Likely to be the wrong way round
            if (smaIntersectionLine.CurrentGradient < emaIntersectionLine.CurrentGradient)
                return EBuyOrSell.Buy;
            else if (smaIntersectionLine.CurrentGradient > emaIntersectionLine.CurrentGradient)
                return EBuyOrSell.Sell;
            else
                return null;
        }
        
        #region private helpers

        private decimal GetGradientDiff(IntersectionLine[] intersectionLines)
        {
            if (intersectionLines.Length != 2)
                throw new Exception($"Wrong number of intersection lines setup. Sould have 2 but {intersectionLines.Length} have been setup.");

            return Math.Abs(intersectionLines[0].CurrentGradient - intersectionLines[1].CurrentGradient);
        }

        private bool DoLinesIntersect(IntersectionLine[] intersectionLines)
        {
            decimal? currentValue = null;

            foreach(IntersectionLine line in intersectionLines)
            {
                if (!currentValue.HasValue)
                    currentValue = line.CurrentValue;
                else if (line.CurrentValue != currentValue)
                    return false;

                return true;
            }

            return false;
        }

        #endregion
    }
}
